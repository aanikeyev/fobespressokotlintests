package com.fobespressotest.android.espresso.test

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText

class MainActivity : Activity() {

    private lateinit var editText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        editText = findViewById<View>(R.id.inputField) as EditText
    }


    fun onClick(view: View) {
        when (view.id) {
            R.id.changeText -> editText.setText(R.string.fobtest_app_testtext)
            R.id.switchActivity -> {
                val intent = Intent(this, SecondActivity::class.java)
                intent.putExtra("input", editText.text.toString())
                startActivity(intent)
            }
        }

    }
}