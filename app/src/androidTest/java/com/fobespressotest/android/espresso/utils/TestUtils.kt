package com.fobespressotest.android.espresso.utils

import android.app.Instrumentation
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import android.support.test.runner.lifecycle.Stage
import android.view.View
import org.hamcrest.Matcher
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.view.ViewGroup

/**
 * Test utils for UI tests implementation
 */
object TestUtils {

    /**
     * Gets [Instrumentation] instrumentation
     */
    private val getInstrumentation: Instrumentation
        get() = InstrumentationRegistry.getInstrumentation()

    /**
     * Gets [String] resource value by given resource id from 'strings.xml'
     */
    fun getTextValue(resourceId: Int): String = getInstrumentation.targetContext.getString(resourceId)

    /**
     * Change screen orientation by given orientation id
     */
    fun changeScreenOrientation(orientation: Int): ViewAction {

        return object: ViewAction {
            override fun getConstraints(): Matcher<View> {
                return ViewMatchers.isRoot()
            }

            override fun getDescription(): String {
                return String.format("change value to: - [%s]", orientation)
            }

            override fun perform(uiController: UiController, view: View) {
                uiController.loopMainThreadUntilIdle()
                val activity = getActivityContext(view.context)
                activity?.run {
                    requestedOrientation = orientation
                }?: run {
                    getActivityView(view)!!.requestedOrientation = orientation
                }
                val resumedActivities = ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
                if (resumedActivities.isEmpty()) {
                    throw RuntimeException("Could not change value")
                }
            }

            private fun getActivityView(view: View): Activity? {
                var activity: Activity? = null
                val v = view as ViewGroup
                val c = v.childCount
                var i = 0
                while (i < c && activity == null) {
                    activity = getActivityContext(v.getChildAt(i).context); ++i
                }
                return activity
            }

            private fun getActivityContext(context: Context): Activity? {
                var context = context
                while (context is ContextWrapper) {
                    if (context is Activity) {
                        return context
                    }
                    context = context.baseContext
                }
                return null
            }
        }
    }
}