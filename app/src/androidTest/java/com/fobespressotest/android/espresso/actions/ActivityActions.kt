package com.fobespressotest.android.espresso.actions

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.pressBack
import android.support.test.espresso.matcher.ViewMatchers.isRoot

/**
 * Activities UI tests related actions
 */
object ActivityActions {

    /**
     * Press back button on device
     */
    fun clickBackButton(): ViewInteraction = onView(isRoot()).perform(pressBack())
}