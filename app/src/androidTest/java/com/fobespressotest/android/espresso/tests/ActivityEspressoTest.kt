package com.fobespressotest.android.espresso.tests

import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
import android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
import android.support.test.espresso.Espresso.*
import android.support.test.espresso.action.ViewActions
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4

import org.junit.runner.RunWith

import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*

import org.junit.runners.MethodSorters
import com.fobespressotest.android.espresso.actions.ActivityActions.clickBackButton
import com.fobespressotest.android.espresso.selectors.MainActivitySelectors.onMainActivityChangeText
import com.fobespressotest.android.espresso.selectors.MainActivitySelectors.onMainActivityInputField
import com.fobespressotest.android.espresso.selectors.MainActivitySelectors.onMainActivitySwitchButton
import com.fobespressotest.android.espresso.selectors.SecondActivitySelectors.onSecondActivityResultView
import com.fobespressotest.android.espresso.test.MainActivity
import com.fobespressotest.android.espresso.test.R
import com.fobespressotest.android.espresso.utils.TestUtils.changeScreenOrientation
import com.fobespressotest.android.espresso.utils.TestUtils.getTextValue
import org.junit.Before
import org.junit.After
import org.junit.FixMethodOrder
import org.junit.Rule
import org.junit.Test

/**
 * Espresso UI tests to check activities
 */
@RunWith(AndroidJUnit4::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ActivityEspressoTest {

    @Rule
    @JvmField
    var mActivityRule = ActivityTestRule(MainActivity::class.java)

    // Screen orientations test list
    private val orientationsList = listOf(SCREEN_ORIENTATION_LANDSCAPE, SCREEN_ORIENTATION_PORTRAIT)

    @Before
    fun startUp() {
        // Set screen orientation to default screen view before test
        onView(isRoot()).perform(changeScreenOrientation(SCREEN_ORIENTATION_PORTRAIT))
    }

    @After
    fun cleanUp() {
        // Set screen orientation to default screen view after test
        onView(isRoot()).perform(changeScreenOrientation(SCREEN_ORIENTATION_PORTRAIT))
    }

    @Test
    fun testMainActivity_shouldDisplayContent() {
        // Check Landscape and Portrait screen orientations
        orientationsList.iterator().forEach {
            // Check screen orientation to Landscape view
            onView(isRoot()).perform(changeScreenOrientation(it))
            // Check input field exists on main activity view
            onMainActivityInputField.check(matches(isDisplayed()))
            // Check  exists on main activity view
            onMainActivityChangeText.check(matches(isDisplayed()))
            // Check switch activity button on main activity view
            onMainActivitySwitchButton.check(matches(isDisplayed()))
        }
    }

    @Test
    fun testEnsureText_shouldChangeTextValue() {
        // Check Landscape and Portrait screen orientations
        orientationsList.iterator().forEach {
            // Check screen orientation to Landscape view
            onView(isRoot()).perform(changeScreenOrientation(it))
            // Type text and then press the button.
            onMainActivityInputField.perform(typeText(getTextValue(R.string.fobtest_app_hello)), closeSoftKeyboard())
            // Click on change text button
            onMainActivityChangeText.perform(click())
            // Check that the test text was changed and displayed
            onMainActivityInputField.check(matches(withText(getTextValue(R.string.fobtest_app_testtext))))
            // Type text and then press the button
            onMainActivityInputField.perform(ViewActions.clearText())
        }
    }

    @Test
    fun testSwitchActivity_shouldChangeActivity() {
        // Check Landscape and Portrait screen orientations
        orientationsList.iterator().forEach {
            // Check screen orientation to Landscape view
            onView(isRoot()).perform(changeScreenOrientation(it))
            // Type text and then press the button
            onMainActivityInputField.perform(typeText(getTextValue(R.string.fobtest_app_newtext)), closeSoftKeyboard())
            // Click on switch activity button
            onMainActivitySwitchButton.perform(click())
            // Check second activity
            onSecondActivityResultView.check(matches(withText(R.string.fobtest_app_newtext)))
            // Return back by tapping on back button
            clickBackButton()
            // Type text and then press the button
            onMainActivityInputField.perform(ViewActions.clearText())
        }
    }
}