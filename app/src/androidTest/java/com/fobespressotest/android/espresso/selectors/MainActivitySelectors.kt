package com.fobespressotest.android.espresso.selectors

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.matcher.ViewMatchers
import com.fobespressotest.android.espresso.test.R

/**
 * Main Activity class file
 */
object MainActivitySelectors {

    /**
     * @return The [ViewInteraction] input field on main activity view
     */
    val onMainActivityInputField: ViewInteraction
        get() = onView(ViewMatchers.withId(R.id.inputField))

    /**
     * @return The [ViewInteraction] change text on main activity view
     */
    val onMainActivityChangeText: ViewInteraction
        get() = onView(ViewMatchers.withId(R.id.changeText))

    /**
     * @return The [ViewInteraction] change text on main activity view
     */
    val onMainActivitySwitchButton: ViewInteraction
        get() = onView(ViewMatchers.withId(R.id.switchActivity))

}