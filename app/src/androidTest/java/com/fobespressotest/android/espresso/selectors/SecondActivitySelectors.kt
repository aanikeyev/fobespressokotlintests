package com.fobespressotest.android.espresso.selectors

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.matcher.ViewMatchers.withId
import com.fobespressotest.android.espresso.test.R

/**
 * Second activity related selectors
 */
object SecondActivitySelectors {

    /**
     * @return The [ViewInteraction] result view on second activity view
     */
    val onSecondActivityResultView: ViewInteraction get() = onView(withId(R.id.resultView))

}