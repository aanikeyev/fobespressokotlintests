# FOBEspressoKotlinTests

FOB Solutions test task implementation using Espresso test automation framework and Kotlin programming language

Download the project, preferably using 'git clone'
git clone https://aanikeyev@bitbucket.org/aanikeyev/fobespressokotlintests.git

The application under test is located in src/main/java

Tests are located in src/androidTest/java

Running all tests using right click mouse above 'class ActivityEspressoTest'
and click Run 'ActivityEspressoTest'

Running any of tests separately using right click mouse above 'testMainActivity_shouldDisplayContent()',
'testEnsureText_shouldChangeTextValue()' or 'testSwitchActivity_shouldChangeActivity'
and click Run 'testMainActivity_shouldDisplayContent()', Run 'testEnsureText_shouldChangeTextValue()'
or Run 'testSwitchActivity_shouldChangeActivity()'